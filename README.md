```
git-filter-repo --filename-callback '
  if filename and filename.startswith(b"metadata/") and filename.count(b".") > 2:
    t = filename.split(b".")
    return t[0] + b"/" + t[1] + filename[8:]
  elif filename and filename.startswith(b"metadata/"):
    t = filename.split(b".")
    return t[0] + filename[8:]
  else:
    return filename
  ' --force --invert-paths --path-glob "metadata/*/" --path build --path stats
```
